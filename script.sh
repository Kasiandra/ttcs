#!/bin/bash

# Create folders for config files
mkdir -p config
mkdir -p query
mkdir -p podcasts

# Create blacklist and downloadlist files
blacklist="query/blacklist.txt"
if [[ ! -f $blacklist ]]; then
	touch $blacklist
fi

query="query/query.txt"
if [[ ! -f $query ]]; then
	touch $query
fi

# Create default config
defaultconf="config/default.conf"
if [[ ! -f $defaultconf ]]; then
	touch $defaultconf
	echo 	"#This is a default config
Channelname='' #Enter here the Channelname you want
SearchTitle='' #Enter what you're looking for at Channelname
Format='' #following options are allowed: mp3, vorbis, opus, aac, m4a, wav
"  > $defaultconf 
fi

# Create script

echo "#!/bin/bash
cd $(pwd)" > start.sh

echo "
url='https://www.youtube.com/feeds/videos.xml?user='

for f in config/*;do 
	source" '$f'"; 
		if [[ ! "'$SearchTitle'" ]]; then
			curl -s "'$url$Channelname'" | xmlstarlet pyx |sed -n '/(title/,/)link/p' | sed 's/)link/\)link;/g' |  tr -d '\n' | tr ';' '\n' | sed 's#^.*linkAhref##' | sed 's/alternate)link//g' | sed 's/Arel//g' | sed 's/[[:blank:]]//g' >> query/query.txt;
			curl -s https://www.youtube.com/feeds/videos.xml?user="'$Channelname'" | grep '<uri>' | sed 's/<uri>//g' | sed 's/<\/uri>//g' | sed 's/[[:blank:]]//g' | uniq >> query/blacklist.txt;
		else
			curl -s "'$url$Channelname'" | xmlstarlet pyx |sed -n '/(title/,/)link/p' | sed 's/)link/\)link;/g' |  tr -d '\n' | tr ';' '\n' | grep "'$SearchTitle'" | sed 's#^.*linkAhref##' | sed 's/alternate)link//g' | sed 's/Arel//g' | sed 's/[[:blank:]]//g' >> query/query.txt;
			curl -s https://www.youtube.com/feeds/videos.xml?user="'$Channelname'" | grep '<uri>' | sed 's/<uri>//g' | sed 's/<\/uri>//g' | sed 's/[[:blank:]]//g' | uniq  >> query/blacklist.txt;
		fi
done 

egrep -v '^\s*$|^#' query/query.txt > query/.query.txt
mv query/.query.txt query/query.txt

while read line2; do
	output="'"$line2"'"
	while read line1; do
		if [ "'"$line2"'" = "'"$line1"'" ]; then
			output=
			break;
		fi
	done < query/blacklist.txt
	printf '%s\n' "'"$output"'"
	done < query/query.txt > query/tmpls
mv query/tmpls query/query.txt
cat query/query.txt >> query/blacklist.txt

youtube-dl --output "'"podcast/%(title)s.%(ext)s"'" -x --audio-format "'$Format'" --batch-file='query/query.txt'
echo '' > query/query.txt

cat query/blacklist.txt | sort | uniq > query/.blacklist.txt
mv query/.blacklist.txt query/blacklist.txt
grep -v '^$' query/blacklist.txt > query/.blacklist.txt
mv query/.blacklist.txt query/blacklist.txt" >> start.sh
