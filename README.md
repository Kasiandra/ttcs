# ttcs
Dies ist ein Bashscript, welches aus einem RSS Feed bei YouTube, nach einem String sucht und diese Dateien herunterlädt, sowie als mp3,ogg,.. umwandelt.

Zum installieren in ein Verzeichnis deiner wahl wechseln und folgendes ins Terminal eingeben:

### curl -L http://git.elektrollart.org/elektroll/ttcs/raw/branch/master/script.sh | bash

Es werden Drei Ordner erstellt, sowie ein script namens **start.sh**.
Im Verzeichnis conifg befindet sich eine Datei, welche als Vorlage dient, in welcher der YouTube Channelname, Suchkriterium und gewünschtes Audio Format eingetragen werden kann. Das Script geht jede .conf Datei in config/ durch und lädt dann die Audio Dateien in podcast/ herunter.

**Abhänigkeiten**
* xmlstarlet
* youtube-dl
* ffmpeg
